.. plantuml::
   :caption: Caption with **bold** and *italic*

   Bob -> Alice: hello
   Alice -> Bob: hi
   !include https://gitlab.com/arihantar/plantuml/-/raw/master/test.puml
